package de.notepass.ttws.api.java;

import de.notepass.ttws.api.java.pojo.TimetableRequestResult;

import java.io.IOException;

public interface TimetableAsyncResponse {
    public void onSuccess(TimetableRequestResult result);
    public void onError(Exception e);
}
