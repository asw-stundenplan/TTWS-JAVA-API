package de.notepass.ttws.api.java.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * THis object contains all days for a week
 */
public class TimetableWeek implements Iterable {
    private ArrayList<TimetableDay> days = new ArrayList<TimetableDay>();
    private Date startDate;
    private Date endDate;

    public Iterator iterator() {
        return days.iterator();
    }

    public void forEach(Consumer action) {
        days.forEach(action);
    }

    public Spliterator spliterator() {
        return days.spliterator();
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public ArrayList<TimetableDay> getDays() {
        return days;
    }
}
