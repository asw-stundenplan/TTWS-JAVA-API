package de.notepass.ttws.api.java.pojo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Consumer;
import java.util.regex.Pattern;

/**
 * This object contains all hours inside a day
 */
public class TimetableDay implements Iterable {
    private Date date;
    private ArrayList<TimetableHour> hours = new ArrayList<TimetableHour>();

    public Date getDate() {
        return date;
    }

    public void addHour(TimetableHour hour) {
        hours.add(hour);
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Iterator iterator() {
        return hours.iterator();
    }

    public void forEach(Consumer action) {
        hours.forEach(action);
    }

    public Spliterator spliterator() {
        return hours.spliterator();
    }

    public ArrayList<TimetableHour> getHours() {
        return hours;
    }
}
