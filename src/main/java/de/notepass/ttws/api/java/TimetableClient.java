package de.notepass.ttws.api.java;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.notepass.ttws.api.java.pojo.Timetable;
import de.notepass.ttws.api.java.pojo.TimetableRequestResult;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class TimetableClient {
    private String clazz;
    private int block;
    private boolean removeEmptyWeeks;
    private String baseUrl = "https://v1.api.asw-stundenplan.de/timetable";

    public TimetableClient(String clazz, int block, boolean removeEmptyWeeks) {
        this.block = block;
        this.removeEmptyWeeks = removeEmptyWeeks;
        this.clazz = clazz;
    }

    public TimetableClient(String clazz, boolean removeEmptyWeeks) {
        this(clazz, -1, removeEmptyWeeks);
    }

    public TimetableClient(String clazz, int block) {
        this(clazz, block, false);
    }

    public TimetableClient(String clazz) {
        this(clazz, -1, false);
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getClazz() {
        return clazz;
    }

    public int getBlock() {
        return block;
    }

    public boolean isRemoveEmptyWeeks() {
        return removeEmptyWeeks;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public TimetableRequestResult getTimetable() throws IOException {
        String url = baseUrl + "/get/outputFormat/JSON/class/"+clazz;
        if (removeEmptyWeeks) {
            url += "/removeEmptyWeeks/true";
        }

        if (block > 0) {
            url += "/block/"+block;
        }

        String json = readStringFromURL(url);

        GsonBuilder builder = new GsonBuilder();
        String dateTimeFormat = "dd-MM-yyyy-HH-mm";
        builder.setDateFormat(dateTimeFormat);
        Gson gson = builder.create();

        return gson.fromJson(json, TimetableRequestResult.class);
    }

    protected static String readStringFromURL(String requestURL) throws IOException {
        try (Scanner scanner = new Scanner(new URL(requestURL).openStream(), StandardCharsets.UTF_8.toString())) {
            scanner.useDelimiter("\\A");
            return scanner.hasNext() ? scanner.next() : "";
        }
    }

    public static void getTimetableAsync(String clazz, int block, TimetableAsyncResponse onCompleted) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                TimetableClient client = new TimetableClient(clazz, block, true);
                try {
                    onCompleted.onSuccess(client.getTimetable());
                } catch (Exception e) {
                    onCompleted.onError(e);
                }
            }
        }).start();
    }

    public static void getTimetableAsync(String clazz, TimetableAsyncResponse onCompleted) {
        getTimetableAsync(clazz, -1, onCompleted);
    }

    public static TimetableRequestResult getTimetable(String clazz, int block) throws IOException {
        TimetableClient client = new TimetableClient(clazz, block, true);
        return client.getTimetable();
    }

    public static TimetableRequestResult getTimetable(String clazz) throws IOException {
        return getTimetable(clazz, -1);
    }
}
