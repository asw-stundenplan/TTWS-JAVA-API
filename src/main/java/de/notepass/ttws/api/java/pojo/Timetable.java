package de.notepass.ttws.api.java.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * This object contains all timetable weeks
 */
public class Timetable implements Iterable {
    private ArrayList<TimetableWeek> weeks = new ArrayList<TimetableWeek>();
    private Date startDate;
    private Date endDate;

    public Iterator iterator() {
        return weeks.iterator();
    }

    public void forEach(Consumer action) {
        weeks.forEach(action);
    }

    public Spliterator spliterator() {
        return weeks.spliterator();
    }

    public ArrayList<TimetableWeek> getWeeks() {
        return weeks;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }
}
