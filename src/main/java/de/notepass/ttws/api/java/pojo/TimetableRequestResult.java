package de.notepass.ttws.api.java.pojo;

/**
 * This class contains the response metadata
 */
public class TimetableRequestResult {
    private boolean successful = true;
    private String errorMessage = null;
    private Timetable timetable = null;

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Timetable getTimetable() {
        return timetable;
    }

    public void setTimetable(Timetable timetable) {
        this.timetable = timetable;
    }
}
