import de.notepass.ttws.api.java.TimetableAsyncResponse;
import de.notepass.ttws.api.java.TimetableClient;
import de.notepass.ttws.api.java.pojo.Timetable;
import de.notepass.ttws.api.java.pojo.TimetableRequestResult;

import java.io.IOException;

public class SimpleRequestTest {
    public static void main(String[] args) throws IOException {
        TimetableClient client = new TimetableClient("WIA18", 8, true);
        TimetableRequestResult t = client.getTimetable();
        System.out.println(t.getTimetable().getStartDate());

        TimetableClient.getTimetableAsync("WIA18", 8, new TimetableAsyncResponse() {
            @Override
            public void onSuccess(TimetableRequestResult result) {
                System.out.println("ASYNC DONE: "+result.getTimetable().getWeeks().get(0).getStartDate());
            }

            @Override
            public void onError(Exception e) {
                System.out.println("ASYNC ERROR: "+e.getMessage());
            }
        });

        System.out.println("SYNC DONE: "+TimetableClient.getTimetable("WIA18", 8).getTimetable().getStartDate());
    }
}
