# Timetablewebservice Java Client
This is the reference implementation for the ASW-Stundenplan.de Webservice client
(Timetablewebservice-Client). It allows to query data from the webservice and automatically
deserializes it.
## Usage
The library is simply to use and functionality is currently somewhat limited.  
There are 3 possibilities to use the client:  
- Create a new Instance of the class de.notepass.ttws.api.java.TimetableClient
and use the getTimetable()-function
- Use the static de.notepass.ttws.api.java.TimetableClient#getTimetableAsync()
method to request data in a asynchronous fashion and without creating a new class
instance
- Use the static de.notepass.ttws.api.java.TimetableClient#getTimetable() to 
request data in the usual, blocking, way
The only possibility to change the base-URL from which the data is requested
is to create a new instance of de.notepass.ttws.api.java.TimetableClient and call
setBaseUrl(String baseUrl). In future releases there will also be possibilities
to change the base URL for the static methods.  

## Maven
This library is currently not available on the maven central repository, but will
be soon. Until then, just download it and use "mvn install"
